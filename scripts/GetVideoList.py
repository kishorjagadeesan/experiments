# Get Movie List
import os
from os import listdir
from os.path import isfile, isdir, join, splitext, abspath, dirname
import re
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "quickchat.settings")
#from qchat.models import Videos

class FilmList:

	dir_name = ''

	drive_list = [
		"F:\\Movies\\",
		"K:\\Movies\\",
	]

	allowed_files = [
		'.avi',
		'.mp4',
		'.mkv',
	]

	languages = {
		'Malayalam' : {},
		'English'   : {},
		'Tamil'		: {},
		'Hindi'		: {},
	}

	def __init__(self, path):
		self.dir_name = path


	def inList(self, item):
		for list_item in self.allowed_files:
			if list_item == item:
				return 1
		return 0

	def get_file_list(self, path):
		lang_folder = ''
		file_list = [files for files in listdir(path) if isfile(join(path, files)) and self.inList(splitext(files)[1])]
		for lang in self.languages.keys():
			if re.search(lang.lower(), str(path).lower()):
				lang_folder = lang
		for item in file_list:
			self.languages[lang_folder][item] = path
		dir_list = [directory for directory in listdir(path) if isdir(join(path, directory))]
		for item in dir_list:
			try:
				self.get_file_list(join(path, item))
			except:
				continue

print(os.environ['DJANGO_SETTINGS_MODULE'])
film_obj = FilmList(dirname(abspath(__file__)))
for drive in film_obj.drive_list:
	film_obj.get_file_list(drive);
for lang in sorted(film_obj.languages.keys()):
	for key, val in sorted(film_obj.languages[lang].items()):
		if not Videos.objects.filter(name=key).exists():
			#video_obj = Videos(	name=key, language=lang, path=val,)
			#video_obj.save()
			pass


'''
fh = open(join(film_obj.dir_name, 'film_list.txt'), 'w')
for lang in sorted(film_obj.languages.keys()):
	fh.write("===============================================\n")
	fh.write(str(lang).upper() + " - " + str(len(film_obj.languages[lang])) +"\n")
	fh.write("===============================================\n")
	for key, val in sorted(film_obj.languages[lang].items()):
		fh.write("{:100} - {} \n".format(key, val))
	fh.write("\n\n")
fh.close()
'''
