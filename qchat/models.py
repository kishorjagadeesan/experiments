from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

class UserDetails(models.Model):
	gender  = models.CharField(null=True, blank=True, max_length=10)
	dob	    = models.DateField(null=True, blank = True)
	user = models.OneToOneField(User)

	def CanCreateUser(email):
		if User.objects.filter(username=email).exists():
			return 1

	def AddNewUser(userDetails):
		try:
			user = User.objects.create_user(first_name = userDetails['first_name'], 
						last_name = userDetails['last_name'],
						email = userDetails['user_email'],
						username = userDetails['user_email'],
						password = userDetails['new_password'])
			return user
		except:
			print('error occured');
class UserAddress(models.Model):
	street = models.CharField(max_length=100)
	street2 = models.CharField(max_length=100, blank=True, null=True)
	city = models.CharField(max_length=50)
	state = models.CharField(max_length=50)
	zipcode = models.CharField(max_length=20)
	landmark = models.CharField(max_length=50, null=True, blank=True)
	user = models.ForeignKey(User)

class Videos(models.Model):
	name = models.CharField(max_length= 300)
	language = models.CharField(max_length=50)
	path = models.CharField(max_length=200)
	category = models.CharField(max_length=200)
	tags = models.CharField(max_length=200)
	details = models.CharField(max_length = 200)
	format = models.CharField(max_length=20)