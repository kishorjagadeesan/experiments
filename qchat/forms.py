# forms.py
from django import forms

class NewUserForm(forms.Form):
	first_name 		 = forms.CharField()
	last_name   	 = forms.CharField()
	user_email		 = forms.EmailField()
	new_password 	 = forms.CharField()
	confirm_password = forms.CharField()

'''	def clean_new_password(self):
		print(self.cleaned_data['new_password'])
		new_password = self.cleaned_data['new_password']
		if len(new_password) < 5:
			self.add_error('new_password', 'Password must be atleast 5 characters')

	def clean_confirm_password(self):
		print(str(self.cleaned_data['new_password']) + str(self.cleaned_data['confirm_password']))
		if self.cleaned_data['new_password'] != self.cleaned_data['confirm_password']:
			pass#self.add_error('confirm_password', 'Confirm password doesn''t match')
'''