# Create your views here.

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from qchat.forms import NewUserForm
from qchat.models import UserDetails

# Login Function
def do_login(request):
	if request.method == 'GET':
		if request.user.id:
			return redirect('/quickchat/home')
		else:
			return render(request, 'login.html')
	elif request.method == 'POST':
		page = {}
		username = request.POST.get('username')
		password = request.POST.get('password')
		user = authenticate(username = username, password = password)

		if user is not None:
			if user.is_active:
				login(request, user)
				if request.GET.get('next'):
					return redirect(request.GET.get('next'))
				return redirect ('/quickchat/home')
			else:
				page['error'] = 'This user is not active'
		else:
			page['error'] = 'Invalid Username or Password'
		return render(request, 'login.html', page)


def logout_user(request):
	if request.user:
		logout(request)
		return redirect('/quickchat/login')
	else:
		return redirect('/quickchat/login')

@login_required()
def user_home (request):
	page = {}
	return render(request, 'home.html', page)

def new_user(request):

	page = {}
	if request.method == 'GET':
		return render(request, 'register.html', page)
	elif request.method == 'POST':
		new_user_form = NewUserForm(request.POST)
		if new_user_form.is_valid():
			clean_data = new_user_form.cleaned_data
			if UserDetails.CanCreateUser(clean_data['user_email']):
				page['create_error'] = 'Email already exist.'
			else:
				new_user = UserDetails.AddNewUser(clean_data)
				if new_user:
					created_user = authenticate(username=clean_data['user_email'], password=clean_data['new_password'])
					login(request, created_user)
					return redirect('/quickchat/home')
				else:
					page['create_error'] = 'Some error occured, please try again after sometime.'
		else:
			page['form_error'] = new_user_form
		return render(request, 'register.html', page)