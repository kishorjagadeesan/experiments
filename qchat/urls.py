# QuickChat URL configuration

from django.conf.urls import include, url
from qchat.views import *

urlpatterns = [
    url(r'login/', do_login),
    url(r'logout/', logout_user),
    url(r'home/', user_home),
    url(r'register/', new_user),
]